/**
 * Main entry point of the program.
 *
 * Basically, it retrieves the arguments provided by the user and launch the
 * simulation, either by printing it to stdout as a simple liste of states, or
 * by using the interactive mode.
 *
 * @author Alexandre Blondin Massé
 */
#include <stdio.h>
#include "parse_args.h"
#include "cellular.h"
#include "interactive.h"

int main(int argc, char **argv) {
    struct Arguments *arguments = parse_arguments(argc, argv);
    if (arguments->status != TP2_OK) {
        return arguments->status;
    } else {
        struct CellularAutomaton *automaton, *next;
        automaton = Cellular_init(arguments->num_rows,
                                  arguments->num_cols,
                                  arguments->type,
                                  arguments->boundary,
                                  arguments->allowed_cells);
        Cellular_set_random(automaton, arguments->distribution);
        if (arguments->interactive) {
            struct InteractiveApplication *application =
                Interactive_init(automaton, arguments->num_steps);
            Interactive_run(application);
            Interactive_free(application);
        } else {
            for (unsigned int step = 0; step < arguments->num_steps; ++step) {
                printf("Step %d\n", step);
                Cellular_print(automaton, false);
                next = Cellular_next(automaton);
                Cellular_free(automaton);
                automaton = next;
            }
        }
        Cellular_free(automaton);
    }
    free_arguments(arguments);
    return TP2_OK;
}
