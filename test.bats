#!/usr/bin/env bats

# Temporary directory for tests

mkdir -p "./tmp"
BATS_TMPDIR="./tmp"

@test "Default program working" {
  run bin/tp2
  [ "$status" -eq 0 ]
  [ "${lines[0]}" = "Step 0" ]
}

@test "Wrong row value" {
  run bin/tp2 -r a
  [ "$status" -eq 1 ]
  [ "${lines[0]}" = "Error: the number of rows, columns and steps must be positive integers." ]
}

@test "Wrong column value" {
  run bin/tp2 -r 5 -c -4
  [ "$status" -eq 1 ]
  [ "${lines[0]}" = "Error: the number of rows, columns and steps must be positive integers." ]
}

@test "Wrong simulation type" {
  run bin/tp2 -t gameoflife
  [ "$status" -eq 2 ]
  [ "${lines[0]}" = "Error: unrecognized simulation type." ]
}

@test "Type alone is not sufficient" {
  run bin/tp2 -t game-of-life
  [ "$status" -eq 6 ]
  [ "${lines[0]}" = "Error: The type and the allowed cells must be set at the same time." ]
}

@test "Game of life with allowed cells" {
  run bin/tp2 -t game-of-life -a ab
  [ "$status" -eq 0 ]
}

@test "Game of life: inconsistent number of cells" {
  run bin/tp2 -t game-of-life -a abc
  [ "$status" -eq 6 ]
  [ "${lines[0]}" = "Error: The simulation and the allowed cells are inconsistent." ]
}

@test "Wrap-around is not a boundary" {
  run bin/tp2 -b wrap-around
  [ "$status" -eq 3 ]
}

@test "Truncate is a valid boundary" {
  run bin/tp2 -b truncate
  [ "$status" -eq 0 ]
}

@test "Periodic is a valid boundary" {
  run bin/tp2 -b periodic
  [ "$status" -eq 0 ]
}

@test "Wrong distribution" {
  run bin/tp2 -d 1,a
  [ "$status" -eq 4 ]
}

@test "Wrong option" {
  run bin/tp2 -x
  [ "$status" -eq 7 ]
}
